<?php

function verifCombi($symbole) { // $symbole vaut 'X' ou 'O'
    global $tab, $statut;
    $statut = 2; // Si on arrive dans cette fonction, le jeu est toujours en cours, donc $statut = 2
    $cases = []; // Tableau de stockage des index pour un symbole

    foreach ($tab as $key => $value) { // On met dans un tableau tous les index pour un symbole donné
        if ($value == $symbole) {
            $cases[] = $key;
        }
    }

    $combinaisons = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

    foreach ($combinaisons as $tabCombi) {
        $nbBons = 0; // Nombre de chiffres trouvés dans le tableau $cases.

        foreach ($tabCombi as $value) {
            if (in_array($value, $cases)) { // Si on trouve le chiffre dans le tableau $cases, alors on ajoute 1 à $nbBons
                $nbBons++;
            }

            if ($nbBons == 3) { // Si $nbBons est à 3, on a trouvé les 3 chiffres de la combinaison dans le tableau $cases
                $statut = 1; // On passe la valeur de statut à 1 car c'est gagné
                break;
            }
        }
    }
    return $statut;
}